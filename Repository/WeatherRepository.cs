using System;
using System.Linq;
using MiniProfilerDemo.Domain;
using MiniProfilerDemo.Interface.IRepository;

namespace MiniProfilerDemo.Repository
{
    public class WeatherRepository : IWeatherRepository
    {
        private static readonly string[] Summaries =
        {
            "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
        };

        public WeatherForecast[] GetData()
        {
            var rng = new Random();

            var result = Enumerable.Range(1, 5)
                                   .Select
                                   (
                                       index => new WeatherForecast
                                       {
                                           Date    = DateTime.Now.AddDays(index), TemperatureC = rng.Next(-20, 55)
                                         , Summary = Summaries[rng.Next(Summaries.Length)]
                                       }
                                   )
                                   .ToArray();

            return result;
        }
    }
}