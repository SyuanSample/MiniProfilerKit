using Microsoft.AspNetCore.Mvc;
using StackExchange.Profiling;

namespace MiniProfilerDemo.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class MiniProfilerDemoController : ControllerBase
    {
        /// <summary>
        /// 獲取html片段
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("GetHtml")]
        public IActionResult GetHtml()
        {
            var html = MiniProfiler.Current.RenderIncludes(HttpContext);
            return Ok(html.Value);
        }
    }
}
