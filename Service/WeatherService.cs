using System;
using System.Linq;
using System.Threading;
using MiniProfilerDemo.Domain;
using MiniProfilerDemo.Interface.IRepository;
using MiniProfilerDemo.Interface.IService;

namespace MiniProfilerDemo.Service
{
    public class WeatherService : IWeatherService
    {
        private readonly IWeatherRepository _repo;

        public WeatherService(IWeatherRepository repo)
        {
            _repo = repo;
        }

        /// <summary>
        ///     強制等待10，驗證功能。
        /// </summary>
        /// <returns></returns>
        public WeatherForecast[] GetData()
        {
            var result = _repo.GetData();

            Thread.Sleep(5000);

            return result;
        }
        
    }
}