using MiniProfilerDemo.Domain;

namespace MiniProfilerDemo.Interface.IRepository
{
    public interface IWeatherRepository
    {
        public WeatherForecast[] GetData();
    }
}