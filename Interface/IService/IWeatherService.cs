using MiniProfilerDemo.Domain;

namespace MiniProfilerDemo.Interface.IService
{
    public interface IWeatherService
    {
        public WeatherForecast[] GetData();
    }
}