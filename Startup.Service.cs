using Microsoft.Extensions.DependencyInjection;
using MiniProfilerDemo.Interface.IRepository;
using MiniProfilerDemo.Interface.IService;
using MiniProfilerDemo.Repository;
using MiniProfilerDemo.Service;

namespace MiniProfilerDemo
{
    public partial class Startup
    {
        public static void RegisterService(IServiceCollection services)
        {
            services.AddScoped<IWeatherService, WeatherService>();
            services.AddScoped<IWeatherRepository, WeatherRepository>();
            
        }
    }
}