using Microsoft.Extensions.DependencyInjection;
using StackExchange.Profiling.Storage;
using System;

namespace MiniProfilerDemo
{
    public partial class Startup
    {
        public static void RegisterMiniProfilere(IServiceCollection services)
        {
            services.AddMiniProfiler(options =>
                    {
                        //訪問地址路由根目錄；預設為：/mini-profiler-resources
                        options.RouteBasePath = "/profiler";
                        //資料快取時間
                        ( options.Storage as MemoryCacheStorage ).CacheDuration = TimeSpan.FromMinutes(60);
                        //sql格式化設定
                        options.SqlFormatter = new StackExchange.Profiling.SqlFormatters.InlineFormatter();
                        //跟蹤連線開啟關閉
                        options.TrackConnectionOpenClose = true;
                        //介面主題顏色方案;預設淺色
                        options.ColorScheme = StackExchange.Profiling.ColorScheme.Dark;
                        //.net core 3.0以上：對MVC過濾器進行分析
                        options.EnableMvcFilterProfiling = true;
                        //對檢視進行分析
                        options.EnableMvcViewProfiling = true;

                        //控制訪問頁面授權，預設所有人都能訪問
                        //options.ResultsAuthorize;
                        //要控制分析哪些請求，預設說有請求都分析
                        //options.ShouldProfile;

                        //內部異常處理
                        //options.OnInternalError = e => MyExceptionLogger(e);
                    })
                    // AddEntityFramework是要監控EntityFrameworkCore生成的SQL
                    .AddEntityFramework();
        }
    }
}